import json
import asyncio

from report import Report
from exchange import Exchange


if __name__ == '__main__':
    exchange_list = []
    # load exchange config in json
    conf_data = json.load(open('conf.json'))

    loop = asyncio.get_event_loop()

    # load exchange config to class
    for exhcange_conf in conf_data:
        exchange_list.append(Exchange(exhcange_conf))

    # task group formation for download data
    tasks = (exchange.update_data() for exchange in exchange_list)
    loop.run_until_complete(asyncio.gather(*tasks))

    # extract pair in data
    tasks = (exchange.extract_pair() for exchange in exchange_list)
    loop.run_until_complete(asyncio.gather(*tasks))

    pair_name_list = ['BTC-LTC', 'LTC-ETH', 'BTC-ETH']
    # print report
    report = Report(exchange_list, pair_name_list)
    report.print_report()

import json
import aiohttp
from decimal import Decimal


class Exchange:
    """Class for work with the exchange"""
    exchange_name = None
    exchange_url = None
    path = None
    params = None
    endpoints = None
    data = None
    coins_pair_dict = None

    def __init__(self, exchange_conf):
        self.exchange_name = exchange_conf.get('ExchangeName')
        self.exchange_url = exchange_conf.get('ExchangeUrl')
        self.path = exchange_conf.get('path')
        self.params = exchange_conf.get("params")
        self.endpoints = exchange_conf.get("endpoints")
        self.coins_pair_dict = {}

    async def update_data(self):
        """
        function make request config for exchange connect and call request_exchange
        :return:None
        """
        self.data = []
        params = {}
        iter_items = ()
        if self.params:
            iter_items = self.params
        elif self.endpoints:
            iter_items = self.endpoints
        for item in iter_items:
            if '{}' in self.exchange_url:
                url = self.exchange_url.format(item)
            else:
                url = self.exchange_url
            if isinstance(item, dict):
                params = item
            await self.request_exchange(url, params)
        if not self.params and not self.endpoints:
            await self.request_exchange(self.exchange_url, self.params)

    async def request_exchange(self, url: str, params: list):
        """
        function make request, transform request->json->dict and save to date
        :param url:
        :param params:
        :return: None
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params) as resp:
                if resp.status == 200:
                    data = await resp.text()
                    self.data.append(json.loads(data))

    async def extract_pair(self):
        """
        function use config path for extract last cost pair in data
        :return:None
        """
        cursor = self.data
        for key, path in self.path.items():
            for item in path.split('/'):
                if isinstance(cursor, list):
                    item = int(item)
                cursor = cursor[item]
            self.coins_pair_dict.update({key: Decimal(cursor)})
            cursor = self.data
        ltc_eth = self.coins_pair_dict.get('BTC-LTC')/self.coins_pair_dict.get('BTC-ETH')
        self.coins_pair_dict.update({'LTC-ETH': ltc_eth})

    def get_pair_by_name(self, pair_name: str) -> dict:
        return {self.exchange_name: self.coins_pair_dict.get(pair_name)}

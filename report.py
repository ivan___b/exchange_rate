"""Report class"""


class Report:
    """ Class generating report"""
    pair_name_list = None
    exchange_list = None

    def __init__(self, exchange_list: list, pair_name_list: list):
        self.pair_name_list = pair_name_list
        self.exchange_list = exchange_list

    def load_exchange_info(self, pair_dict: dict, pair_name: str):
        """
        load exchange par info to dict
        :param pair_dict:
        :param pair_name:
        :return: None
        """
        for exchange in self.exchange_list:
            pair_dict.update(exchange.get_pair_by_name(pair_name))

    @staticmethod
    def out_message(cost_type: str, cost_key: str, pair_dict: dict, pair_name: str):
        """
        output min and max cost pair and pair info
        :param cost_type: str 'min' or 'max'
        :param cost_key: str example 'poloniex'
        :param pair_dict: dict of exchange name: pait cost
        :param pair_name: str examle 'BTC-LTC'
        :return:
        """
        cost = ""
        costs = ''.join(['     {} - {}\n'.format(key, item) for key, item in pair_dict.items()])
        if cost_type == 'min':
            cost = "min cost {} - {}\n".format(cost_key, pair_dict.get(cost_key))
        elif cost_type == 'max':
            cost = "max cost {} - {}\n".format(cost_key, pair_dict.get(cost_key))
        info = ("    pair - {} cost type - {}\n"
                "    Exchange - cost\n"
                "{}\n"
                "    {}\n").format(pair_name, cost_type, costs, cost)
        print(info)

    def output_cost_pair(self, pair_name: str, cost_type: str):
        """
        found min or max cost
        :param pair_name:
        :param cost_type:
        :return: None
        """
        cost_key = ''
        pair_dict = {}
        self.load_exchange_info(pair_dict, pair_name)
        if cost_type == 'min':
            cost_key = min(pair_dict, key=pair_dict.get)
        elif cost_type == 'max':
            cost_key = max(pair_dict, key=pair_dict.get)
        self.out_message(cost_type, cost_key, pair_dict, pair_name)

    def print_report(self):
        """
        :return:None
        """
        for pair in self.pair_name_list:
            self.output_cost_pair(pair, 'min')
        for pair in self.pair_name_list:
            self.output_cost_pair(pair, 'max')
